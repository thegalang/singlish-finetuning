import pandas as pd
import numpy as np
import sys
from tqdm.auto import tqdm
import os

import torch
import torch.nn as nn
from torch.nn.functional import softmax
from torchmetrics.functional import accuracy
from torch.utils.data import Dataset, DataLoader, Sampler
from datasets import load_dataset

from transformers import ElectraTokenizerFast, ElectraModel, ElectraForSequenceClassification, ElectraForMaskedLM, TextDataset
import pytorch_lightning as pl


class ElectraSmall(pl.LightningModule):

    def __init__(self):
        super().__init__()

        try:
            num_labels = int(os.environ["NUM_CLASS_LABELS"])
        except:
            raise("Please set NUM_CLASS_LABELS environment variable to be the number of classification label")
        
        #self.tokenizerGenerator = tokenizerGenerator
        self.generator = ElectraForMaskedLM.from_pretrained("google/electra-small-generator") # this is ignored
        self.discriminator = ElectraForSequenceClassification.from_pretrained("google/electra-small-discriminator", num_labels = num_labels)
        

    # saat masuk, labels berisi original label
    def forward(self, input_ids=None, attention_mask=None, token_type_ids = None, labels = None):
        
        ret = self.discriminator(input_ids = input_ids, attention_mask = attention_mask, token_type_ids = token_type_ids, labels = labels)
        return ret[0], ret[1]
    

    def training_step(self, batch, batch_idx):
        
        input_ids = batch["input_ids"]
        attention_mask = batch["attention_mask"]
        token_type_ids = batch["token_type_ids"]
        labels = batch["labels"]

        loss, outputs = self(input_ids, attention_mask, token_type_ids, labels)
        output_labels = torch.argmax(outputs, dim = 1)
        # print("input_ids", input_ids)
        # print("attention_mask", attention_mask)
        # print("token_type_ids", token_type_ids)
        # print("labels", labels)
        # print("output_labels", output_labels)
        #print(loss)
        # get label 
        
        acc = accuracy(output_labels, labels)
        self.log("train_loss", loss, prog_bar=True, logger=True)
        self.log("train_acc", acc,  prog_bar=True, logger=True)
        return {"loss": loss, "train_acc": acc,  "predictions": outputs.detach(), "pred_labels": output_labels, "true_labels": labels}
    
    def validation_step(self, batch, batch_idx):

        input_ids = batch["input_ids"]
        attention_mask = batch["attention_mask"]
        token_type_ids = batch["token_type_ids"]
        labels = batch["labels"]

        loss, outputs = self(input_ids, attention_mask, token_type_ids,  labels)

        output_labels = torch.argmax(outputs, dim = 1)
        acc = accuracy(output_labels, labels)
        self.log("val_loss", loss, prog_bar=True, logger=True)
        self.log("val_acc", acc, prog_bar=True, logger=True)
        return {"val_loss": loss, "val_acc": acc,  "predictions": outputs.detach(), "pred_labels": output_labels, "true_labels": labels}

    def test_step(self, batch, batch_idx):
        input_ids = batch["input_ids"]
        attention_mask = batch["attention_mask"]
        token_type_ids = batch["token_type_ids"]
        labels = batch["labels"]

        loss, outputs = self(input_ids, attention_mask, token_type_ids,  labels)
        output_labels = torch.argmax(outputs, dim = 1)
        acc = accuracy(output_labels, labels)
        self.log("test_acc", acc, prog_bar=True, logger=True)
        return {"test_acc": acc, "predictions": outputs.detach(), "pred_labels": output_labels, "true_labels": labels}


    def predict_step(self, batch, batch_idx):
        input_ids = batch["input_ids"]
        attention_mask = batch["attention_mask"]
        token_type_ids = batch["token_type_ids"]
        labels = batch["labels"]
        data_ids = batch["rowId"]

        #print(len(input_ids), len(attention_mask), len(token_type_ids), len(labels), len(data_ids))
        _, outputs = self(input_ids, attention_mask, token_type_ids,  labels)
        output_labels = torch.argmax(outputs, dim = 1)
        
        return (data_ids, output_labels)
        
    # calculate all loss
    def training_epoch_end(self, training_outputs):
        pred_labels, true_labels = [], []
        for output in training_outputs:
            pred_labels.append(output["pred_labels"])
            true_labels.append(output["true_labels"])

        pred_labels = torch.cat(pred_labels, dim = 0)
        true_labels = torch.cat(true_labels, dim = 0)
        whole_acc = accuracy(pred_labels, true_labels)
        # print("data train acc", pred_labels, len(pred_labels))
        # print("data train acc value", whole_acc.item())
        self.log("data_train_acc", whole_acc.item(), prog_bar=True, logger=True)

    def validation_epoch_end(self, training_outputs):
        pred_labels, true_labels = [], []
        for output in training_outputs:
            pred_labels.append(output["pred_labels"])
            true_labels.append(output["true_labels"])

        pred_labels = torch.cat(pred_labels, dim = 0)
        true_labels = torch.cat(true_labels, dim = 0)
        whole_acc = accuracy(pred_labels, true_labels)
        # print("data train acc", pred_labels, len(pred_labels))
        # print("data train acc value", whole_acc.item())
        self.log("data_val_acc", whole_acc.item(), prog_bar=True, logger=True)


        # sch = self.lr_schedulers()
        # print("HUHA", sch)
        # # If the selected scheduler is a ReduceLROnPlateau scheduler.
        # if isinstance(sch, torch.optim.lr_scheduler.ReduceLROnPlateau):
        #     print("MASHOK SCHEDULER PAK EKO")
        #     sch.step(whole_acc)

    def test_epoch_end(self, training_outputs):
        pred_labels, true_labels = [], []
        for output in training_outputs:
            pred_labels.append(output["pred_labels"])
            true_labels.append(output["true_labels"])

        pred_labels = torch.cat(pred_labels, dim = 0)
        true_labels = torch.cat(true_labels, dim = 0)
        whole_acc = accuracy(pred_labels, true_labels)
        # print("data train acc", pred_labels, len(pred_labels))
        # print("data train acc value", whole_acc.item())
        self.log("data_test_acc", whole_acc.item(), prog_bar=True, logger=True)

    def configure_optimizers(self):
        opt1 = torch.optim.Adam(self.parameters(), lr=0.000003, eps=0.000001, betas=(0.9, 0.999))
        #lrPlateau = torch.optim.lr_scheduler.ReduceLROnPlateau(opt1, mode='max', factor=0.5, patience = 3)
        return opt1

class ElectraBase(pl.LightningModule):

    def __init__(self):
        super().__init__()

        
        try:
            num_labels = int(os.environ["NUM_CLASS_LABELS"])
        except:
            raise("Please set NUM_CLASS_LABELS environment variable to be the number of classification label")

        #self.tokenizerGenerator = tokenizerGenerator
        self.generator = ElectraForMaskedLM.from_pretrained("google/electra-base-generator") # this is ignored
        self.discriminator = ElectraForSequenceClassification.from_pretrained("google/electra-base-discriminator", num_labels = num_labels)
        

    # saat masuk, labels berisi original label
    def forward(self, input_ids=None, attention_mask=None, token_type_ids = None, labels = None):
        
        ret = self.discriminator(input_ids = input_ids, attention_mask = attention_mask, token_type_ids = token_type_ids, labels = labels)
        return ret[0], ret[1]
    

    def training_step(self, batch, batch_idx):
        
        input_ids = batch["input_ids"]
        attention_mask = batch["attention_mask"]
        token_type_ids = batch["token_type_ids"]
        labels = batch["labels"]

        loss, outputs = self(input_ids, attention_mask, token_type_ids, labels)
        output_labels = torch.argmax(outputs, dim = 1)
        # print("input_ids", input_ids)
        # print("attention_mask", attention_mask)
        # print("token_type_ids", token_type_ids)
        # print("labels", labels)
        # print("output_labels", output_labels)
        #print(loss)
        # get label 
        
        acc = accuracy(output_labels, labels)
        self.log("train_loss", loss, prog_bar=True, logger=True)
        self.log("train_acc", acc,  prog_bar=True, logger=True)
        return {"loss": loss, "train_acc": acc,  "predictions": outputs.detach(), "pred_labels": output_labels, "true_labels": labels}
    
    def validation_step(self, batch, batch_idx):

        input_ids = batch["input_ids"]
        attention_mask = batch["attention_mask"]
        token_type_ids = batch["token_type_ids"]
        labels = batch["labels"]

        loss, outputs = self(input_ids, attention_mask, token_type_ids,  labels)

        output_labels = torch.argmax(outputs, dim = 1)
        acc = accuracy(output_labels, labels)
        self.log("val_loss", loss, prog_bar=True, logger=True)
        self.log("val_acc", acc, prog_bar=True, logger=True)
        return {"val_loss": loss, "val_acc": acc,  "predictions": outputs.detach(), "pred_labels": output_labels, "true_labels": labels}

    def test_step(self, batch, batch_idx):
        input_ids = batch["input_ids"]
        attention_mask = batch["attention_mask"]
        token_type_ids = batch["token_type_ids"]
        labels = batch["labels"]

        loss, outputs = self(input_ids, attention_mask, token_type_ids,  labels)
        output_labels = torch.argmax(outputs, dim = 1)
        acc = accuracy(output_labels, labels)
        self.log("test_acc", acc, prog_bar=True, logger=True)
        return {"test_acc": acc, "predictions": outputs.detach(), "pred_labels": output_labels, "true_labels": labels}


    def predict_step(self, batch, batch_idx):
        input_ids = batch["input_ids"]
        attention_mask = batch["attention_mask"]
        token_type_ids = batch["token_type_ids"]
        labels = batch["labels"]
        data_ids = batch["rowId"]

        #print(len(input_ids), len(attention_mask), len(token_type_ids), len(labels), len(data_ids))
        _, outputs = self(input_ids, attention_mask, token_type_ids,  labels)
        output_labels = torch.argmax(outputs, dim = 1)
        
        return (data_ids, output_labels)

    # calculate all loss
    def training_epoch_end(self, training_outputs):
        pred_labels, true_labels = [], []
        for output in training_outputs:
            pred_labels.append(output["pred_labels"])
            true_labels.append(output["true_labels"])

        pred_labels = torch.cat(pred_labels, dim = 0)
        true_labels = torch.cat(true_labels, dim = 0)
        whole_acc = accuracy(pred_labels, true_labels)
        # print("data train acc", pred_labels, len(pred_labels))
        # print("data train acc value", whole_acc.item())
        self.log("data_train_acc", whole_acc.item(), prog_bar=True, logger=True)

    def validation_epoch_end(self, training_outputs):
        pred_labels, true_labels = [], []
        for output in training_outputs:
            pred_labels.append(output["pred_labels"])
            true_labels.append(output["true_labels"])

        pred_labels = torch.cat(pred_labels, dim = 0)
        true_labels = torch.cat(true_labels, dim = 0)
        whole_acc = accuracy(pred_labels, true_labels)
        # print("data train acc", pred_labels, len(pred_labels))
        # print("data train acc value", whole_acc.item())
        self.log("data_val_acc", whole_acc.item(), prog_bar=True, logger=True)


        # sch = self.lr_schedulers()
        # print("HUHA", sch)
        # # If the selected scheduler is a ReduceLROnPlateau scheduler.
        # if isinstance(sch, torch.optim.lr_scheduler.ReduceLROnPlateau):
        #     print("MASHOK SCHEDULER PAK EKO")
        #     sch.step(whole_acc)

    def test_epoch_end(self, training_outputs):
        pred_labels, true_labels = [], []
        for output in training_outputs:
            pred_labels.append(output["pred_labels"])
            true_labels.append(output["true_labels"])

        pred_labels = torch.cat(pred_labels, dim = 0)
        true_labels = torch.cat(true_labels, dim = 0)
        whole_acc = accuracy(pred_labels, true_labels)
        # print("data train acc", pred_labels, len(pred_labels))
        # print("data train acc value", whole_acc.item())
        self.log("data_test_acc", whole_acc.item(), prog_bar=True, logger=True)

    def configure_optimizers(self):
        opt1 = torch.optim.Adam(self.parameters(), lr=0.000003, eps=0.000001, betas=(0.9, 0.999))
        #lrPlateau = torch.optim.lr_scheduler.ReduceLROnPlateau(opt1, mode='max', factor=0.5, patience = 3)
        return opt1