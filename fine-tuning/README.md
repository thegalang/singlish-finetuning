# Files explanation:
- finetune.py: to finetune (train - validate - test) on task
- finetune_test.py: to only do test on task
- finetune_predict.py: to do inference job on task. Will generate an additional csv with "pred_labels" field

Currently only support sequence classification tasks.
Do set the environment variable NUM_CLASS_LABELS=x, where x is the number of labels in your sequence classification problem.

# Parameters explanation:
-  MODEL_NAME: one of the available implementations:ELECTRA_SMALL, ELECTRA_BASE, BERT_BASE_EN, SINGBERT_ZANELIM
-  CORPUS_TRAIN: path file of train corpus
-  CORPUS_VAL: path file of validation corpus
-  CORPUS_TEST: path file of test corpus
-  N_EPOCS: number of epoch
-  CHECKPOINT_DIR: where to save the checkpoints
-  LIGHTNING_LOGS_DIR: where to save the lightning logs
-  GPU: gpu id to use (example, gpu = 0 will use gpu 0) 
-  CHECKPOINT_PATH: model checkpoint to load
-  TRAINING_CHECKPOINT_PATH: training checkpoint to load, if want to continue training from certain checkpoint

# Examples

## Finetuning

### sentiment analysis
 

ELECTRA_SMALL: python finetune.py ELECTRA_SMALL sentiment-analysis/sing-sentiment-train-v1.csv sentiment-analysis/sing-sentiment-val-v1.csv sentiment-analysis/sing-sentiment-test-v1.csv 15 sentiment-analysis/electra_small electra_small_sentiment 1 singElectra_small.ckpt


ELECTRA_BASE: python finetune.py ELECTRA_BASE sentiment-analysis/sing-sentiment-train-v1.csv sentiment-analysis/sing-sentiment-val-v1.csv sentiment-analysis/sing-sentiment-test-v1.csv 15 sentiment-analysis/electra_base electra_base_sentiment 1 singElectra_base.ckpt

BERT_BASE_EN: python finetune.py BERT_BASE_EN sentiment-analysis/sing-sentiment-train-v1.csv sentiment-analysis/sing-sentiment-val-v1.csv sentiment-analysis/sing-sentiment-test-v1.csv 15 sentiment-analysis/bert_base_en bert_base_sentiment 1

SINGBERT_ZANELIM: python finetune.py SINGBERT_ZANELIM sentiment-analysis/sing-sentiment-train-v1.csv sentiment-analysis/sing-sentiment-val-v1.csv sentiment-analysis/sing-sentiment-test-v1.csv 15 sentiment-analysis/singbert_zanelim singbert_zanelim_sentiment 1


## singlish identification

ELECTRA_SMALL: python finetune.py ELECTRA_SMALL singlish-identification/sing-iden-train-v2.csv singlish-identification/sing-iden-val-v2.csv singlish-identification/sing-iden-test-v2.csv 15 singlish-identification/electra_small electra_small_identification 1 singElectra_small.ckpt

ELECTRA_BASE: python finetune.py ELECTRA_BASE singlish-identification/sing-iden-train-v2.csv singlish-identification/sing-iden-val-v2.csv singlish-identification/sing-iden-test-v2.csv 15 singlish-identification/electra_base electra_base_identification 1 singElectra_base.ckpt

BERT_BASE: python finetune.py BERT_BASE_EN singlish-identification/sing-iden-train-v2.csv singlish-identification/sing-iden-val-v2.csv singlish-identification/sing-iden-test-v2.csv 15 singlish-identification/bert_base_en bert_base_en_identification 1

SINGBERT_ZANELIM: python finetune.py SINGBERT_ZANELIM singlish-identification/sing-iden-train-v2.csv singlish-identification/sing-iden-val-v2.csv singlish-identification/sing-iden-test-v2.csv 15 singlish-identification/singbert_zanelim singbert_zanelim_identification 1


## Testing: 

### sentiment analysis

ELECTRA_SMALL: python finetune_test.py ELECTRA_SMALL sentiment-analysis/sing-sentiment-train-v1.csv sentiment-analysis/sing-sentiment-val-v1.csv sentiment-analysis/sing-sentiment-test-v1.csv 0 sentiment-analysis/electra_small/model-epoch9-train0.5405929684638977-val0.5399113297462463.ckpt

ELECTRA_BASE: python finetune_test.py ELECTRA_BASE sentiment-analysis/sing-sentiment-train-v1.csv sentiment-analysis/sing-sentiment-val-v1.csv sentiment-analysis/sing-sentiment-test-v1.csv 0 sentiment-analysis/electra_base/model-epoch5-train0.7777777910232544-val0.6917960047721863.ckpt

BERT_BASE_EN: python finetune_test.py BERT_BASE_EN sentiment-analysis/sing-sentiment-train-v1.csv sentiment-analysis/sing-sentiment-val-v1.csv sentiment-analysis/sing-sentiment-test-v1.csv 0 sentiment-analysis/bert_base_en/model-epoch4-train0.7551953196525574-val0.6607539057731628.ckpt

ZANELIM_SINGBERT: python finetune_test.py SINGBERT_ZANELIM sentiment-analysis/sing-sentiment-train-v1.csv sentiment-analysis/sing-sentiment-val-v1.csv sentiment-analysis/sing-sentiment-test-v1.csv 0 sentiment-analysis/singbert_zanelim/model-epoch7-train0.8344416618347168-val0.6807095408439636.ckpt

### singlish identification

ELECTRA_SMALL: python finetune_test.py ELECTRA_SMALL singlish-identification/sing-iden-train-v2.csv singlish-identification/sing-iden-val-v2.csv singlish-identification/sing-iden-test-v2.csv 0 singlish-identification/electra_small/model-epoch15-train0.9216542840003967-val0.9257642030715942.ckpt

ELECTRA_BASE: python finetune_test.py ELECTRA_BASE singlish-identification/sing-iden-train-v2.csv singlish-identification/sing-iden-val-v2.csv singlish-identification/sing-iden-test-v2.csv 0 singlish-identification/electra_base/model-epoch2-train0.9451307058334351-val0.9405021667480469.ckpt

BERT_BASE: python finetune_test.py BERT_BASE_EN singlish-identification/sing-iden-train-v2.csv singlish-identification/sing-iden-val-v2.csv singlish-identification/sing-iden-test-v2.csv 0 singlish-identification/bert_base_en/model-epoch0-train0.8869173526763916-val0.9470524191856384.ckpt

SINGBERT_ZANELIM: python finetune_test.py SINGBERT_ZANELIM singlish-identification/sing-iden-train-v2.csv singlish-identification/sing-iden-val-v2.csv singlish-identification/sing-iden-test-v2.csv 0 singlish-identification/singbert_zanelim/model-epoch4-train0.9686753749847412-val0.9475982785224915.ckpt


## inference

### sentiment analysis:

ELECTRA_BASE: python finetune_predict.py ELECTRA_BASE sentiment-analysis/sing-sentiment-test-v1.csv 1 sentiment-analysis/electra_base/model-epoch5-train0.7777777910232544-val0.6917960047721863.ckpt electra_base_senti.csv

SINGBERT_ZANELIM: python finetune_predict.py SINGBERT_ZANELIM sentiment-analysis/sing-sentiment-test-v1.csv 1 sentiment-analysis/singbert_zanelim/model-epoch7-train0.8344416618347168-val0.6807095408439636.ckpt singbert_zanelim_senti.csv

### singlish identification:

ELECTRA_BASE: python finetune_predict.py ELECTRA_BASE singlish-identification/sing-iden-test-v2.csv 0 singlish-identification/electra_base/model-epoch2-train0.9451307058334351-val0.9405021667480469.ckpt electra_base_iden.csv

SINGBERT_ZANELIM: python finetune_predict.py SINGBERT_ZANELIM singlish-identification/sing-iden-test-v2.csv 0 singlish-identification/singbert_zanelim/model-epoch1-train0.9415819048881531-val0.9388646483421326.ckpt singbert_zanelim_iden.csv