import pandas as pd
import numpy as np
import sys
from tqdm.auto import tqdm

from SinglishDatamodule import SinglishDataModule

from transformers import ElectraTokenizerFast, BertTokenizerFast
from Electra import ElectraSmall, ElectraBase
from Bert import BertBase, BertZanelim
from transformers.data import data_collator
import random

import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping
from pytorch_lightning.loggers import TensorBoardLogger
import os
import torch

class CheckpointEveryEpoch(pl.Callback):
    def __init__(self, start_epoc, dir_path):
        self.start_epoc = start_epoc
        self.dir_path = dir_path

    def on_train_epoch_end(self, trainer: pl.Trainer, _):
        """ Check if we should save a checkpoint after every train epoch """
        epoch = trainer.current_epoch
        if epoch >= self.start_epoc:
            filename = f'{self.dir_path}/model-epoch{epoch}'
            train_acc = "NaN"
            if "data_train_acc" in trainer.logged_metrics:
                train_acc = trainer.logged_metrics["data_train_acc"]
            val_acc = "NaN"
            if "data_val_acc" in trainer.logged_metrics:
                val_acc = trainer.logged_metrics["data_val_acc"]
            
            if train_acc == "NaN" or val_acc == "NaN":
              return

            filename = f'model-epoch{epoch}-train{train_acc}-val{val_acc}.ckpt'
            ckpt_path = f'{self.dir_path}/{filename}'
            trainer.save_checkpoint(ckpt_path)
            
if __name__ == '__main__':
  #torch.multiprocessing.freeze_support()

  torch.cuda.empty_cache()

  os.environ["TOKENIZERS_PARALLELISM"] = "false"

  path = '.'
  max_seq_length = 128

  MODEL_NAME = sys.argv[1] # values: ELECTRA_SMALL, ELECTRA_BASE, BERT_BASE_EN, SINGBERT_ZANELIM
  CORPUS_TRAIN= sys.argv[2]
  CORPUS_VAL = sys.argv[3]
  CORPUS_TEST = sys.argv[4]
  GPU = int(sys.argv[5])

  CHECKPOINT_PATH = None
  if len(sys.argv) >= 7:
    CHECKPOINT_PATH = sys.argv[6]


  model = None
  tokenizer = None
  if MODEL_NAME == "ELECTRA_SMALL":
    model = ElectraSmall()
    #model.load_from_checkpoint(CHECKPOINT_PATH, strict=False)
    tokenizer = ElectraTokenizerFast(vocab_file = f"vocab.txt", max_length=128)
  elif MODEL_NAME == "ELECTRA_BASE":
    model = ElectraBase()
    tokenizer = ElectraTokenizerFast(vocab_file = f"vocab.txt", max_length=128)
  elif MODEL_NAME == "BERT_BASE_EN":
    model = BertBase()
    tokenizer = BertTokenizerFast.from_pretrained('bert-base-cased')
  elif MODEL_NAME == "SINGBERT_ZANELIM":
    model = BertZanelim()
    tokenizer = BertTokenizerFast(vocab_file = f"vocab.txt", max_length=128)
  else:
    raise("MODEL NAME NOT FOUND")

  
  model = model.load_from_checkpoint(CHECKPOINT_PATH, strict = False)
  print("READ DATASET")
  datamodule = SinglishDataModule({"train" : CORPUS_TRAIN, "val": CORPUS_VAL, "test": CORPUS_TEST}, tokenizer, batch_size=32, max_token_len = 128)
  print("DONE READING DATASET")
  print(len(datamodule.train_dataloader()))

  trainer = pl.Trainer(
    default_root_dir = '.',
    gpus=[GPU],
  )

  trainer.test(model, dataloaders=datamodule.test_dataloader())