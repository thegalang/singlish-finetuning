import torch
import torch.nn as nn
from torch.nn.functional import softmax
from torchmetrics.functional import accuracy
from torch.utils.data import Dataset, DataLoader, Sampler
from datasets import load_dataset
import pandas as pd
import pytorch_lightning as pl
from tqdm.auto import tqdm
import random

def closest_power_of_two(x: int):
  return 1<<(x-1).bit_length()

def log2(x: int):
  return (x-1).bit_length()

# pad to longest sequence
class BatchSampleByLength(Sampler):

  def __init__(self, dataset, batch_size, max_len, drop_last = False):
    indices_by_length = []
    self.batch_size = batch_size
    self.drop_last = False
    size = 1
    while(size <= max_len * 2):
          indices_by_length.append([])
          size *= 2
    
    
    print("batching...")
    for i in tqdm(range(len(dataset))):
      input_ids_len = dataset.getlen(i)
      if log2(input_ids_len) >= len(indices_by_length):
        print(f"[LOGIC ERROR] has log2 {log2(input_ids_len)} while max is {len(indices_by_length)}")
        assert(log2(input_ids_len) < len(indices_by_length))
      #print("HUHA", log2(input_ids_len), input_ids_len, len(self.indices_by_length))
      indices_by_length[log2(input_ids_len)].append(i)

    batches = []
    print("preparing batch iteration...")
    for key in tqdm(range(len(indices_by_length))):
      ids = indices_by_length[key]
      random.shuffle(ids)

      batch_size = self.batch_size
      #batch_of_length = [ids[i:i + batch_size] for i in range(0, len(ids), batch_size)]

      for i in range(0, len(ids), batch_size):
        batches.append(ids[i:i + batch_size])

    random.shuffle(batches)
    self.batches = batches
    self.length = len(batches)
  
  def __iter__(self):
    return iter(self.batches)
    


  def __len__(self):
    return self.length


def collate(batch_list):

  seqlen = len(batch_list[0]["input_ids"]) 
  p2 = closest_power_of_two(seqlen) # all will have the same closes power of 2 due to our BatchSampleByLength
  batch = {}
  #print(batch_list)
  for key in batch_list[0].keys():
    sequences = []
    for itemDict in batch_list:
      item = itemDict[key]
      #print(item.shape[0], p2)

      # do padding if it's not the labels or the row id
      if key != "labels" and key != "rowId":
        item =  nn.ConstantPad1d((0, p2 - item.shape[0]), 0)(item)

      sequences.append(item)
    

    batch[key] = torch.stack(sequences, dim = 0)

  return batch

class SinglishDataset(Dataset):

  def encode(self, example):
      #print("max_seq_len", self.max_seq_len)
      return self.tokenizer(example, max_length = self.max_seq_len, truncation=True, return_tensors='pt')
  
  
  # data: dataset of ['text']
  def __init__(self, data: pd.DataFrame, tokenizer = None,  max_seq_len: int = 128):
    self.tokenizer = tokenizer
    #self.data = data
    self.data = []
    self.labels = []
    self.max_seq_len = max_seq_len
    self.text = []
    for _, row in tqdm(data.iterrows()):
        self.text.append(row["text"])
        self.data.append(self.encode(row["text"]))
        self.labels.append(int(row["label"]))

    #print(self.labels)
    
    
  def __len__(self):
    return len(self.data)

  def getlen(self, i):
    return len(self.data[i]["input_ids"][0])

  def get_text(self, i):
    return self.text[i]

  def __getitem__(self, index: int):
    #data_row = self.data[index]
    encoding = self.data[index]

    #print(len(input_ids), len(attention_mask), len(labels), len(originalWords), len(originalLabels))
    return dict(
      input_ids= encoding["input_ids"][0],
      attention_mask= encoding["attention_mask"][0],
      token_type_ids = encoding["token_type_ids"][0],
      labels = torch.tensor(self.labels[index]),
      rowId = torch.tensor(index)
    )

class SinglishDataModule(pl.LightningDataModule):
  
    
    # expected ada train dan val
    def __init__(self, corpus_paths, tokenizer, batch_size=8, max_token_len=128):
      super().__init__()


      traindf = pd.read_csv(corpus_paths["train"])
      valdf = pd.read_csv(corpus_paths["val"])
      testdf = pd.read_csv(corpus_paths["test"])
      self.max_token_len = max_token_len

      self.train_df = SinglishDataset(traindf, tokenizer, max_token_len)
      self.val_df = SinglishDataset(valdf, tokenizer, max_token_len)
      self.test_df = SinglishDataset(testdf, tokenizer, max_token_len)

      self.batch_size = batch_size

    def setup(self, stage=None):
      pass

    def train_dataloader(self):
      return DataLoader(
        self.train_df,
        collate_fn=collate,
        batch_sampler = BatchSampleByLength(self.train_df, self.batch_size, max_len = self.max_token_len),
        num_workers=8
      )

    def val_dataloader(self):
      return DataLoader(
        self.val_df,
        collate_fn=collate,
        batch_sampler = BatchSampleByLength(self.val_df, self.batch_size, max_len = self.max_token_len), 
        num_workers=4
      )

    def test_dataloader(self):
      return DataLoader(
        self.test_df,
        collate_fn=collate,
        batch_sampler = BatchSampleByLength(self.test_df, self.batch_size, max_len = self.max_token_len),
        num_workers=4,
      )


