import pandas as pd
import numpy as np
import sys
from tqdm.auto import tqdm

import torch
import torch.nn as nn

import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping
from pytorch_lightning.loggers import TensorBoardLogger
import os

from pretrain_singlish_datamodule import SinglishDataModule
from pretrain_electra_model import ElectraSmall, ElectraBase

class CheckpointEveryEpoch(pl.Callback):
    def __init__(self, start_epoc, dir_path):
        self.start_epoc = start_epoc
        self.dir_path = dir_path

    def on_epoch_end(self, trainer: pl.Trainer, _):
        """ Check if we should save a checkpoint after every train epoch """
        epoch = trainer.current_epoch
        if epoch >= self.start_epoc:
            filename = f'{self.dir_path}/model-epoch{epoch}'
            train_loss = "NaN"
            if "train_loss" in trainer.logged_metrics:
                train_loss = trainer.logged_metrics["train_loss"]
            val_loss = "NaN"
            if "val_loss" in trainer.logged_metrics:
                val_loss = trainer.logged_metrics["val_loss"]
            
            if train_loss == "NaN" or val_loss == "NaN":
              return
              
            filename = f'model-epoch{epoch}-train{train_loss}-val{val_loss}.ckpt'
            ckpt_path = f'{self.dir_path}/{filename}'
            trainer.save_checkpoint(ckpt_path)


class CheckpointEveryNSteps(pl.Callback):
    """
    Save a checkpoint every N steps, instead of Lightning's default that checkpoints
    based on validation loss.
    """

    def __init__(
        self,
        checkpoint_dir,
        save_step_frequency,
        prefix="N-Step-Checkpoint",
        use_modelcheckpoint_filename=False,
    ):
        """
        Args:
            save_step_frequency: how often to save in steps
            prefix: add a prefix to the name, only used if
                use_modelcheckpoint_filename=False
            use_modelcheckpoint_filename: just use the ModelCheckpoint callback's
                default filename, don't use ours.
        """
        self.save_step_frequency = save_step_frequency
        self.prefix = prefix
        self.checkpoint_dir = checkpoint_dir
        self.use_modelcheckpoint_filename = use_modelcheckpoint_filename
        self.flip = 0

    def on_batch_end(self, trainer: pl.Trainer, _):
        """ Check if we should save a checkpoint after every train batch """
        epoch = trainer.current_epoch
        global_step = trainer.global_step
        if global_step % self.save_step_frequency == 0:
            if self.use_modelcheckpoint_filename:
                filename = trainer.checkpoint_callback.filename
            else:
                filename = f"{self.prefix}_{self.flip}.ckpt" # nama file alternate _0 _1
                self.flip = (1 - self.flip)
            
            
            ckpt_path = os.path.join(self.checkpoint_dir, filename)
            trainer.save_checkpoint(ckpt_path)


# version 4: only accept encoded tokens
if __name__ == '__main__':
  torch.multiprocessing.freeze_support()

  torch.cuda.empty_cache()

  path = '.'

  CORPUS_TRAIN= sys.argv[1]
  CORPUS_VAL = sys.argv[2]
  MODEL_NAME = sys.argv[3]
  N_EPOCS = int(sys.argv[4])
  MIN_BATCH_SIZE = int(sys.argv[5])
  MAX_BATCH_SIZE = int(sys.argv[6])
  CHECKPOINT_DIR = sys.argv[7]

  CHECKPOINT_PATH = None
  if len(sys.argv) >= 9:
    CHECKPOINT_PATH = sys.argv[8]

  print("READ DATASET")
  datamodule = SinglishDataModule({"train" : CORPUS_TRAIN, "val": CORPUS_VAL}, min_batch_size=MIN_BATCH_SIZE, max_batch_size = MAX_BATCH_SIZE)
  print("DONE READING DATASET")
  #print("testing if can load train datamodule...")
  #print(len(datamodule.train_dataloader()))
  #print("done testing if can load train datamodule")

  logger = TensorBoardLogger(f"lightning_logs", name=MODEL_NAME)

  model = None
  if MODEL_NAME == "ELECTRA_BASE":
    model = ElectraSmall()
  elif MODEL_NAME == "ELECTRA_SMALL":
    model = ElectraSmall()

  if model == None:
    print("MODEL NAME NOT FOUND")

  trainer = pl.Trainer(
    default_root_dir = '.',
    logger=logger,
    callbacks = [pl.callbacks.progress.TQDMProgressBar(refresh_rate=20), CheckpointEveryEpoch(0, CHECKPOINT_DIR), CheckpointEveryNSteps(CHECKPOINT_DIR, 2000)],
    max_epochs=N_EPOCS,
    gpus=[1],
  )

  if CHECKPOINT_PATH is None:
    trainer.fit(model, datamodule)
  else:
    trainer.fit(model, datamodule, ckpt_path = CHECKPOINT_PATH)
  trainer.save_checkpoint(f"{CHECKPOINT_DIR}/final.ckpt")