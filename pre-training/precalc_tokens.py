import pandas as pd
import numpy as np
import sys
from tqdm.auto import tqdm

import torch
import torch.nn as nn
from datasets import load_dataset

from transformers import ElectraTokenizerFast, ElectraModel, ElectraForPreTraining, ElectraForMaskedLM, TextDataset
import random

import os
import pickle

if __name__ == '__main__':

  path = '.'
  max_seq_length = 128

  CORPUS= sys.argv[1]
  OUTPUT = sys.argv[2]
  #OUTPUT = sys.argv[3]


  tokenizer = ElectraTokenizerFast(vocab_file = f"vocab.txt", max_length=128)
  
  tokenized = []
  dataset = load_dataset("text", data_files=CORPUS)
  for line in tqdm(dataset["train"]):

  	if(line['text'] == ''):
  		#print("mashok")
  		continue

  	ret = tokenizer(line['text'], max_length = max_seq_length, truncation=True, return_tensors='pt')
  	retlist = ret["input_ids"][0].tolist()
  	tokenized.append(retlist)

  
  out = open(OUTPUT, "wb")
  pickle.dump(tokenized, out, pickle.HIGHEST_PROTOCOL)
  out.close()
  