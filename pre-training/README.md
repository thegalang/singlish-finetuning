Pretrains electra

# How to Use

1. Prepare `corpus-train.txt` and `corpus-val.txt` in the following format:
```
sentence 1 paragraph 1
sentence 2 paragraph 1
sentence 3 paragraph 1
sentence 4 paragraph 1

sentence 1 paragraph 2
sentence 2 paragraph 2
sentence 3 paragraph 2
sentence 4 paragraph 2
```

Example:
```

According to Ms Suliana, who managed to review the security footage from a nearby CCTV camera, the car thief - whom she described as a medium-built, dark-skinned man - was driving a white Honda car when he was spotted at around 8pm.
He carried out the theft alone in about 20 minutes.

"But the way the thief just brazenly went about his business, in full view of everyone, was scary," she said.
"That was when I read online reports about the recent spate of car thefts and realised that Hondas were an easy target."

```

2. Use `precalc_tokens.py` to convert corpus from text to tokenized version
```

python precalc_tokens.py corpus-train.txt corpus-train-tokenized.txt
python precalc_tokens.py corpus-val.txt corpus-train-tokenized.txt
```

3. Train the model using `pretrain_electra.py`. Args are:
	- CORPUS_TRAIN: the train corpus location
	- CORPUS_VAL: the val corpus location
	- MODEL_NAME: ELECTRA_SMALL/ELECTRA_LARGE
	- N_EPOCS: number of epochs
	- MIN_BATCH_SIZE: minimum batch size
	- MAX_BATCH_SIZE: maximum batch size. Batch size is calculted as min(maximum_batch_size, minimum_batch_size * minimum_batch_size/(max_token_len_in_batch))
	- GPU: the GPU number. (0/1)
	- CHECKPOINT_DIR: where to save the checkpoints
	- CHECKPOINT_PATH: last trained model checkpoint. Will continue training from this checkpoint.

example
```
python pretrain_electra.py corpus-train-tok-128.txt corpus-val-tokenized.txt ELECTRA_SMALL 6 128 1024 1 checkpoints checkpoints/model-last-epoch.ckpt

```

# Features

1. Dynamic batch size. Will group sentences of similar batch size in a larger batch so that training is faster
2. Checkpoints per step and per epoch. Will save last two models, and last model of every epoch
3. Has a lot of constant optimizations to speed up training.

# Issues

Cannot handle multiple GPU, can only run in 1 GPU.
 
