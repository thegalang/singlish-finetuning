import pandas as pd
import numpy as np
import sys
from tqdm.auto import tqdm

import torch
import torch.nn as nn
from torch.nn.functional import softmax
from torch.utils.data import Dataset, DataLoader, Sampler
from datasets import load_dataset
import random

import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping
from pytorch_lightning.loggers import TensorBoardLogger
import os


# pad to longest sequence
def closest_power_of_two(x: int):
  return 1<<(x-1).bit_length()

# get log2 fast
def log2(x: int):
  return (x-1).bit_length()

# pad to longest sequence
class BatchSampleByLength(Sampler):

  def __init__(self, dataset, min_batch_size, max_batch_size, drop_last = False):
    indices_by_length = []
    #self.batch_size = batch_size
    self.drop_last = False
    size = 1
    while(size <= 2*batch_size):
          indices_by_length.append([])
          size *= 2
    
    
    print("batching...")
    for i in tqdm(range(len(dataset))):
      input_ids_len = dataset.getlen(i)
      assert(log2(input_ids_len) < len(indices_by_length))
      #print("HUHA", log2(input_ids_len), input_ids_len, len(self.indices_by_length))
      indices_by_length[log2(input_ids_len)].append(i)

    batches = []
    print("preparing batch iteration...")
    for key in tqdm(range(len(indices_by_length))):
      ids = indices_by_length[key]
      random.shuffle(ids)

      batch_size = min(max_batch_size, min_batch_size * min_batch_size//(1 << key))
      #batch_of_length = [ids[i:i + batch_size] for i in range(0, len(ids), batch_size)]

      for i in range(0, len(ids), batch_size):
        batches.append(ids[i:i + batch_size])

    random.shuffle(batches)
    self.batches = batches
    self.length = len(batches)
  
  def __iter__(self):
    return iter(self.batches)
    


  def __len__(self):
    return self.length


def collate(batch_list):

  seqlen = len(batch_list[0]["input_ids"]) 
  p2 = closest_power_of_two(seqlen) # all will have the same closes power of 2 due to our BatchSampleByLength
  batch = {}
  for key in batch_list[0].keys():
    sequences = []
    for itemDict in batch_list:
      item = itemDict[key]
      #print(item.shape[0], p2)
      item =  nn.ConstantPad1d((0, p2 - item.shape[0]), 0)(item)
      sequences.append(item)
    
    #print("size of sequences", sys.getsizeof(sequences))
    batch[key] = torch.stack(sequences, dim = 0)
  
  #print("size of batch", sys.getsizeof(batch))
  return batch

class SinglishDataset(Dataset):
  
  def read_tokenized_file(self, path):

    res = []
    with open(path, "r") as file:
      print(f"reading {path}")
      for line in tqdm(file):
        line = line.strip()
        res.append(line)
        #print(len(res[-1]))
      print(f"done reading {path}")
    
    #print(res[0], len(res))
    return res

  # data: dataset of ['text']
  def __init__(self, tokenized_path):
    #self.data = data
    self.data = self.read_tokenized_file(tokenized_path)
    
  def __len__(self):
    return len(self.data)

  # fast way getting len without converting to torch.tensor
  def getlen(self, index: int):
    return self.data[index].count(',') + 1


  def __getitem__(self, index: int):
    #data_row = self.data[index]
    input_ids = [int(x) for x in self.data[index].split(',')]
    ln = len(input_ids)
    attention_mask = [1 for _ in range(ln)]
    token_type_ids = [1 for _ in range(ln)]

    return dict(
      input_ids= torch.LongTensor(input_ids),
      attention_mask= torch.LongTensor(attention_mask),
      token_type_ids = torch.LongTensor(token_type_ids),
    )

# wrapper for SinglishDataset so that it's not initialized twice
class SinglishDatasetWrapper(Dataset):

  # data: dataset of ['text']
  def __init__(self, dataset):
    #self.data = data
    self.dataset = dataset
    
  def __len__(self):
    return len(self.dataset)

  # fast way getting len without converting to torch.tensor
  def getlen(self, index: int):
    return self.dataset.getlen(index)


  def __getitem__(self, index: int):
    #data_row = self.data[index]
    return self.dataset[index]

class SinglishDataModule(pl.LightningDataModule):
    
    # expected ada train dan val
    def __init__(self, corpus_paths, min_batch_size=64, max_batch_size = 256):
      super().__init__()


      val = corpus_paths["val"]
      train = corpus_paths["train"]
      
      train_df = SinglishDataset(train)
      self.train_df = SinglishDatasetWrapper(train_df)
      print("done init train")
      val_df = SinglishDataset(val)
      self.val_df = SinglishDatasetWrapper(val_df)

      self.min_batch_size = min_batch_size
      self.max_batch_size = max_batch_size

    def setup(self, stage=None):
      pass

    def train_dataloader(self):
      return DataLoader(
        self.train_df,
        collate_fn=collate,
        batch_sampler = BatchSampleByLength(self.train_df, self.min_batch_size, self.max_batch_size),
        num_workers=3
      )

    def val_dataloader(self):
      return DataLoader(
        self.val_df,
        collate_fn=collate,
        batch_sampler = BatchSampleByLength(self.val_df, self.min_batch_size, self.max_batch_size), 
        num_workers=1
      )

    def test_dataloader(self):
      return DataLoader(
        self.val_df,
        collate_fn=collate,
        batch_sampler = BatchSampleByLength(self.val_df, self.min_batch_size, self.max_batch_size),
        num_workers=0,
      )
