import pandas as pd
import numpy as np
import sys
from tqdm.auto import tqdm

import torch
import torch.nn as nn
from torch.nn.functional import softmax
from transformers import ElectraTokenizerFast, ElectraModel, ElectraForPreTraining, ElectraForMaskedLM, TextDataset
import random

import pytorch_lightning as pl



class ElectraBase(pl.LightningModule):
    
    def __init__(self):
        super().__init__()

        
        #self.tokenizerGenerator = tokenizerGenerator
        self.generator = ElectraForMaskedLM.from_pretrained("google/electra-base-generator")
        self.discriminator = ElectraForPreTraining.from_pretrained("google/electra-base-discriminator")
    
    # saat masuk, labels berisi original label
    def forward(self, input_ids=None, attention_mask=None, token_type_ids = None, labels = None):
        
        #print(input_ids.device, attention_mask.device, token_type_ids.device, labels.device)
        lol = self.generator(input_ids = input_ids, attention_mask = attention_mask, token_type_ids = token_type_ids, labels = labels)
        loss, logits = lol[0], lol[1]
        #print(loss, logits)
        softmaxValue = softmax(logits, dim = 1)
        optToken = torch.argmax(softmaxValue, dim = 2)
        new_inputs = torch.where((input_ids == 103), optToken, input_ids)

        if labels is not None:
          labels = torch.where((labels != -100), (new_inputs != labels).type_as(labels), labels)

        ret = self.discriminator(input_ids = new_inputs, attention_mask = attention_mask, token_type_ids = token_type_ids, labels = labels)
        return ret[0] + loss, ret[1], loss, ret[0]
    
    def mask_input(self, input_ids, attention_mask):
      
      masked = input_ids.clone()

      for i in range(input_ids.shape[0]):
        rand = torch.rand(input_ids[i].shape).to(input_ids.device)
        mask_arr = (rand < 0.15) * (input_ids[i] != 101) * (input_ids[i] != 102) * (attention_mask[i] == 1)
        #print(mask_arr)
        selection = torch.flatten((mask_arr).nonzero()).tolist()
        #print("selection is", selection)
        masked[i, selection] = 103
        #print("====")

      return masked

    def training_step(self, batch, batch_idx):
        
        input_ids = batch["input_ids"]
        attention_mask = batch["attention_mask"]
        token_type_ids = batch["token_type_ids"]
        labels = input_ids.clone()
        labels = torch.where(attention_mask == 1, labels, -100)

        masked_input = self.mask_input(input_ids, attention_mask)

        loss, outputs, mlm_loss, disc_loss = self(masked_input, attention_mask, token_type_ids, labels)
        #print(loss)
        self.log("train_loss", loss, prog_bar=True, logger=True)
        self.log("train_disc_loss", disc_loss, prog_bar = True, logger=True)
        self.log("train_mlm_loss", mlm_loss, prog_bar=True, logger=True)
        return {"loss": loss, "predictions": outputs.detach()}
    
    def validation_step(self, batch, batch_idx):

        input_ids = batch["input_ids"]
        attention_mask = batch["attention_mask"]
        token_type_ids = batch["token_type_ids"]
        labels = input_ids.clone()
        labels = torch.where(attention_mask == 1, labels, -100)

        masked_input = self.mask_input(input_ids, attention_mask)

        loss, outputs, mlm_loss, disc_loss = self(masked_input, attention_mask, token_type_ids, labels)
        #print(loss)
        self.log("val_loss", loss, prog_bar=True, logger=True)
        self.log("val_disc_loss", disc_loss, prog_bar = True, logger=True)
        self.log("val_mlm_loss", mlm_loss, prog_bar=True, logger=True)
        return {"val_loss": loss, "predictions": outputs.detach()}

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters(), lr=0.0000005, eps=0.0000001, betas=(0.9, 0.999))


class ElectraSmall(pl.LightningModule):
    
    def __init__(self):
        super().__init__()

        
        #self.tokenizerGenerator = tokenizerGenerator
        self.generator = ElectraForMaskedLM.from_pretrained("google/electra-base-generator")
        self.discriminator = ElectraForPreTraining.from_pretrained("google/electra-base-discriminator")
    
    # saat masuk, labels berisi original label
    def forward(self, input_ids=None, attention_mask=None, token_type_ids = None, labels = None):
        
        #print(input_ids.device, attention_mask.device, token_type_ids.device, labels.device)
        lol = self.generator(input_ids = input_ids, attention_mask = attention_mask, token_type_ids = token_type_ids, labels = labels)
        loss, logits = lol[0], lol[1]
        #print(loss, logits)
        softmaxValue = softmax(logits, dim = 1)
        optToken = torch.argmax(softmaxValue, dim = 2)
        new_inputs = torch.where((input_ids == 103), optToken, input_ids)

        if labels is not None:
          labels = torch.where((labels != -100), (new_inputs != labels).type_as(labels), labels)

        ret = self.discriminator(input_ids = new_inputs, attention_mask = attention_mask, token_type_ids = token_type_ids, labels = labels)
        return ret[0] + loss, ret[1], loss, ret[0]
    
    def mask_input(self, input_ids, attention_mask):
      
      masked = input_ids.clone()

      for i in range(input_ids.shape[0]):
        rand = torch.rand(input_ids[i].shape).to(input_ids.device)
        mask_arr = (rand < 0.15) * (input_ids[i] != 101) * (input_ids[i] != 102) * (attention_mask[i] == 1)
        #print(mask_arr)
        selection = torch.flatten((mask_arr).nonzero()).tolist()
        #print("selection is", selection)
        masked[i, selection] = 103
        #print("====")

      return masked

    def training_step(self, batch, batch_idx):
        
        input_ids = batch["input_ids"]
        attention_mask = batch["attention_mask"]
        token_type_ids = batch["token_type_ids"]
        labels = input_ids.clone()
        labels = torch.where(attention_mask == 1, labels, -100)

        masked_input = self.mask_input(input_ids, attention_mask)

        loss, outputs, mlm_loss, disc_loss = self(masked_input, attention_mask, token_type_ids, labels)
        #print(loss)
        self.log("train_loss", loss, prog_bar=True, logger=True)
        self.log("train_disc_loss", disc_loss, prog_bar = True, logger=True)
        self.log("train_mlm_loss", mlm_loss, prog_bar=True, logger=True)
        return {"loss": loss, "predictions": outputs.detach()}
    
    def validation_step(self, batch, batch_idx):

        input_ids = batch["input_ids"]
        attention_mask = batch["attention_mask"]
        token_type_ids = batch["token_type_ids"]
        labels = input_ids.clone()
        labels = torch.where(attention_mask == 1, labels, -100)

        masked_input = self.mask_input(input_ids, attention_mask)

        loss, outputs, mlm_loss, disc_loss = self(masked_input, attention_mask, token_type_ids, labels)
        #print(loss)
        self.log("val_loss", loss, prog_bar=True, logger=True)
        self.log("val_disc_loss", disc_loss, prog_bar = True, logger=True)
        self.log("val_mlm_loss", mlm_loss, prog_bar=True, logger=True)
        return {"val_loss": loss, "predictions": outputs.detach()}

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters(), lr=0.0000005, eps=0.0000001, betas=(0.9, 0.999))